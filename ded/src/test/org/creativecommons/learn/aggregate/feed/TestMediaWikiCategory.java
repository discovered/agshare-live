package org.creativecommons.learn.aggregate.feed;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.creativecommons.learn.DiscoverEdTestCase;

public class TestMediaWikiCategory extends DiscoverEdTestCase {
	public void testGetCategoryMembers() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		Set<String> pages = new HashSet<String>(mwc.allPagesInCategory("http://wiki.creativecommons.org/api.php", "Category:DiscoverEd"));
		assertTrue(pages.contains("DiscoverEd Quickstart"));
	}
	
	public void testLotsOfGetCategoryMembers() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		Set<String> pages = new HashSet<String>(mwc.allPagesInCategory("http://en.wikipedia.org/w/api.php", "Category:Biology"));
		assertTrue(pages.size() > 100);
	}
	
	public void testCalculateApiUrlWithCreativeCommonsWiki() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		URL apiURL =  mwc.calculateApiUrl("http://wiki.creativecommons.org/Category:DiscoverEd");
		assertEquals("http://wiki.creativecommons.org/api.php", apiURL.toString());
	}
	
	public void testCalculateApiUrlWithEnglishWikipedia() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		URL apiURL =  mwc.calculateApiUrl("http://en.wikipedia.org/wiki/Category:People");
		assertEquals("http://en.wikipedia.org/w/api.php", apiURL.toString());
	}

}
