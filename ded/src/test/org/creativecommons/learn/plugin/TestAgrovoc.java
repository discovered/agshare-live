package org.creativecommons.learn.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import junit.framework.TestCase;

public class TestAgrovoc extends TestCase {
	public static void testRelatedTermsFromAgrovoc() {
		String ginkgo = "ginkgo biloba";
		Collection<String> related = Agrovoc.getRelatedTermsFromAgrovoc(ginkgo);
		Collection<String> expected = new HashSet<String>();
		expected.add("Oil crops");
		expected.add("maidenhair (tree)");
		expected.add("Ornamental woody plants");
		assertEquals(expected, related);
		
	}
	
	public static void testRelatedTermsParsing() {
		String sample = "[RT, 5328, 12330, 34067]";
		Collection<Integer> expected = new ArrayList<Integer>();
		expected.add(5328);
		expected.add(12330);
		expected.add(34067);
		assertEquals(expected, Agrovoc.parseRTList(sample));

	}
	
	public static void testTranslatedTermsFromAgrovoc() {
		String ginkgo = "ginkgo biloba";
		Collection<String> related = Agrovoc.getTranslations(ginkgo);
		assertTrue(related.contains("은행나무")); // a translation
		assertFalse(related.contains("EN"));    // a language marker
	}
}
