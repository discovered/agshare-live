
<%@ page
  session="false"
  import="java.io.*"
  import="java.util.*"
  import="org.creativecommons.learn.RdfStoreFactory"
  import="org.creativecommons.learn.oercloud.Curator"
%>
<%
  String language =
    ResourceBundle.getBundle("org.nutch.jsp.search", request.getLocale())
    .getLocale().getLanguage();
  String requestURI = HttpUtils.getRequestURL(request).toString();
  String base = requestURI.substring(0, requestURI.lastIndexOf('/'));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
  // To prevent the character encoding declared with 'contentType' page
  // directive from being overriden by JSTL (apache i18n), we freeze it
  // by flushing the output buffer. 
  // see http://java.sun.com/developer/technicalArticles/Intl/MultilingualJSP/
  out.flush();
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/i18n" prefix="i18n" %>
<i18n:bundle baseName="org.nutch.jsp.search"/>
<html lang="<%= language %>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
<title>DiscoverEd: Curators</title>
<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
<jsp:include page="/include/style.html"/>
</head>

<body>
<jsp:include page="<%= \"../\" + language + \"/include/header.html\"%>"/>

<div class="box">

<h1>Curators</h1>

<% Collection<Curator> curators = RdfStoreFactory.get().forDEd().load(Curator.class); %>


<ul>
	<% for (Curator c : curators) { %>
	<li><a href="<%=request.getContextPath()%>/browse/feeds.jsp?c=<%=c.getUrl()%>"><%=c.getName() %></a>  
		<a href="<%=c.getUrl() %>"><img src="<%=request.getContextPath()%>/img/house.png" border="0" /></a>
    <ul><li>provided <%=c.getNumberOfResources()%> resource(s)</li></ul>
	</li>
	<% } %>
</ul>

</div>

<jsp:include page="/footer.jsp"/>

</body>
</html>
