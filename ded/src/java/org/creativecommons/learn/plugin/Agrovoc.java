package org.creativecommons.learn.plugin;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.creativecommons.learn.IndexFieldName;
import org.fao.gilw.aims.webservices.AgrovocWSProxy;
import org.mortbay.log.Log;

/**
 * This class provides an interface to Agrovoc.
 * @author paulproteus
 *
 */
public class Agrovoc {
	private static final AgrovocWSProxy proxy = new AgrovocWSProxy();

	public static Collection<Integer> parseRTList(String rtList) {
		ArrayList<Integer> ret = new ArrayList<Integer>();
		String[] pieces = StringUtils.split(rtList, ",");
		for (String piece: pieces) {
			if (piece.startsWith("[RT")) {
				continue; // this is the header of the list
			}
			ret.add(Integer.parseInt(piece.trim().replace("]", "")));
		}
		return ret;
	}
	
	private static String getTermcodeByTerm(String string) {
		// FIXME: Cache this.
		try {
			return proxy.getTermcodeByTerm(string);
		} catch (RemoteException e) {
			Log.warn("Well, if we can't get the term code, we can just fail here.");
			return null;
		}
	}

	public static Set<String> getRelatedTermsFromAgrovoc(String string) {
		HashSet<String> relatedTermStrings = new HashSet<String>();
		
		// First, get the term code
		String code = getTermcodeByTerm(string);
		if (code == null) {
			return relatedTermStrings; // stop here
		}
				
		// Second, get related terms
		String[] conceptInfo = null;
		try {
			conceptInfo = proxy.getConceptInfoByTermcode(code);
		} catch (RemoteException e) {
			// well, that sucks. the web services gave us an error.
			Log.info("Web services gave us an error.");
			return relatedTermStrings;
		}
		
		Collection<Integer> relatedTermNumbers = new ArrayList<Integer>();

		// find the "Related" section
		for (String maybeRelated: conceptInfo) {
			if (maybeRelated.startsWith("[RT")) {
				relatedTermNumbers = parseRTList(maybeRelated);
				break;
			}
		}
		
		// Turn those into strings
		for (int relatedTermNumber: relatedTermNumbers) {
			String term = Agrovoc.termNumberToString(relatedTermNumber);
			if (term == null) {
				continue; // shucks we didn't get its name
			} else {
				relatedTermStrings.add(term);
			}
		}
		
		return relatedTermStrings;
	}

	private static String termNumberToString(int termNumber) {
		String termName = "";

		try {
			termName = proxy.getTermByLanguage(termNumber, "en");
			return termName;
		} catch (RemoteException e) {
			Log.warn("Yargh, we hit an exception while turning term numbers into strings.");
			return null;
		} 
	}

	public static Set<String> getTranslations(String subject) {
		Set<String> ret = new HashSet<String>();
		String termCodeString = Agrovoc.getTermcodeByTerm(subject);
		if (termCodeString == null) {
			return ret;
		}
		int termNumber = Integer.parseInt(termCodeString);
		
		String labelsMess = "";
		try {
			labelsMess = proxy.getAllLabelsByTermcode2(termNumber, "|");
		} catch (RemoteException e) {
			Log.warn("I expected some labels.");
			return ret;
		}
		
		// The thing we get back looks like
		// translation|LANGUAGE_ID|translation|LANGUAGE_ID
		// so here we parse it.

		HashMap<String, String> languageMap = new HashMap<String, String>();
		boolean onLanguageLabel = false;
		String lastTranslation = null;
		for (String s: StringUtils.split(labelsMess, "|")) {
			if (onLanguageLabel) {
				languageMap.put(s, lastTranslation);
				onLanguageLabel = false;
			}
			else {
				lastTranslation = s;
				onLanguageLabel = true;
			}
		}
		
		// Now, ignore the *keys* and dump the *values* into ret
		ret.addAll(languageMap.values());
		
		return ret;
	}

}
