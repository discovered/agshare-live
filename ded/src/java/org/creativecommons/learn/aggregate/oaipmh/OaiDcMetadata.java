package org.creativecommons.learn.aggregate.oaipmh;
import java.net.URISyntaxException;

import org.creativecommons.learn.RdfStore;
import org.creativecommons.learn.RdfStoreFactory;
import org.creativecommons.learn.feed.IResourceExtractor;
import org.creativecommons.learn.oercloud.Feed;
import org.creativecommons.learn.oercloud.OaiResource;
import org.creativecommons.learn.oercloud.Resource;
import org.dom4j.Element;

import se.kb.oai.OAIException;
import se.kb.oai.pmh.MetadataFormat;
import se.kb.oai.pmh.OaiPmhServer;
import se.kb.oai.pmh.Record;
import thewebsemantic.NotFoundException;

public class OaiDcMetadata extends OaiMetadataFormat implements IResourceExtractor {

/*
	    <element ref="dc:title"/>
	    <element ref="dc:creator"/>
	    <element ref="dc:subject"/>
	    <element ref="dc:description"/>
	    <element ref="dc:publisher"/>
	    <element ref="dc:contributor"/>
	    <element ref="dc:date"/>
	    <element ref="dc:type"/>
	    <element ref="dc:format"/>
	    <element ref="dc:identifier"/>
	    <element ref="dc:source"/>
	    <element ref="dc:language"/>
	    <element ref="dc:relation"/>
	    <element ref="dc:coverage"/>
	    <element ref="dc:rights"/>
*/

	public OaiDcMetadata(MetadataFormat f) {
		super(f);
	}

	@Override
	public void process(RdfStore store, Feed feed, Record oai_record, String identifier) throws OAIException, URISyntaxException {
		// Retrieve the resource metadata from the server
		Element metadata = oai_record.getMetadata();
		if (metadata == null) return;

		// get the namespace prefix
		metadata.addNamespace("dc", "http://purl.org/dc/elements/1.1/");
		
		// load or create the Resource object
		String resource_url = getNodeTextAsUrl(metadata, "//dc:identifier");
		if (resource_url == null) return;
		
		Resource item = getResource(resource_url, store);
		item.getSources().add(feed);

		// title
		item.setTitle(getNodeText(metadata, "//dc:title"));
		
		// creator
		drainIteratorIntoCollection(getNodesText(metadata, "//dc:creator"),
				item.getCreators());
		
		// subject(s)
		drainIteratorIntoCollection(getNodesText(metadata, "//dc:subject"),
				item.getSubjects());
		
		// description
		item.setDescription(getNodeText(metadata, "//dc:description"));
		
		// contributor(s)
		drainIteratorIntoCollection(getNodesText(metadata, "//dc:contributor"),
				item.getContributors());
		
		// format(s)
		drainIteratorIntoCollection(getNodesText(metadata, "//dc:format"),
				item.getFormats());
				
		// language(s)
		drainIteratorIntoCollection(getNodesText(metadata, "//dc:language"),
				item.getLanguages());
		
		// type(s)
		drainIteratorIntoCollection(getNodesText(metadata, "//dc:type"),
				item.getTypes());
		
		// source
		item.getSources().add(feed);
		
		// see also
		try {
			item.getSeeAlso().add(store.load(OaiResource.class, identifier));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// persist the Resource
		store.save(item);
	}

}
