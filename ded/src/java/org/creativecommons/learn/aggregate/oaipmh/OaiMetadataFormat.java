package org.creativecommons.learn.aggregate.oaipmh;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.creativecommons.learn.RdfStore;
import org.creativecommons.learn.feed.IResourceExtractor;
import org.creativecommons.learn.oercloud.Resource;
import org.dom4j.Element;
import org.dom4j.Node;

import se.kb.oai.pmh.MetadataFormat;
import thewebsemantic.NotFoundException;

public abstract class OaiMetadataFormat implements IResourceExtractor{

	protected MetadataFormat format;
	protected RdfStore store;
	
	protected void drainIteratorIntoCollection(Iterator<String> it, Collection<String> collection) {
		while (it.hasNext()) {
			collection.add(it.next());
		}
	}

	public OaiMetadataFormat(MetadataFormat f) {
		super();

		this.format = f; 

	}
	
	public Resource getResource(String url, RdfStore store) throws URISyntaxException {
		
		Resource result = null;
		
		if (store.exists(Resource.class, url)) {
			try {
				result = store.load(Resource.class, url);
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			result = new Resource(url);
		}
		
		return result;
	}

	protected String getNodeText(Element context, String xpath) {
		List<?> identifiers = context.selectNodes(xpath);
		if (identifiers.size() < 1) return null;
		return ((Node) identifiers.get(0)).getText();
	}

	protected String getNodeTextAsUrl(Element context, String xpath) {
		// Return the first node value we come to which can be interpreted as a URL
		Iterator<String> iter = getNodesText(context, xpath);
		while (iter.hasNext()) {
			String nodetext = iter.next();
			try {
				@SuppressWarnings("unused")
				URL node_url = new URL(nodetext);
				
				// successfully created a URL, return this value
				return nodetext;
				
			} catch (MalformedURLException e) {
				
			}
		}
		
		return null;
	}
	protected Iterator<String> getNodesText(Element context, String xpath) {
		class XPathIterator implements Iterator<String> {
			
			private List<Node> items;
			private Iterator<Node> innerIterator;

			@SuppressWarnings("unchecked")
			public XPathIterator(Element context, String xpath) {
				this.items = context.selectNodes(xpath);
				this.innerIterator = items.iterator();
			}

			@Override
			public boolean hasNext() {
				return this.innerIterator.hasNext();
			}

			@Override
			public String next() {
				return this.innerIterator.next().getText();
			}

			@Override
			public void remove() {
				this.innerIterator.remove();
			}
		}
		return new XPathIterator(context, xpath);
	}

}