package org.creativecommons.learn.aggregate.feed;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.creativecommons.learn.DEdConfiguration;
import org.creativecommons.learn.RdfStore;
import org.creativecommons.learn.RdfStoreFactory;
import org.creativecommons.learn.oercloud.Feed;
import org.creativecommons.learn.oercloud.Resource;
import org.creativecommons.learn.plugin.MetadataRetrievers;

import com.sun.syndication.feed.module.DCModule;
import com.sun.syndication.feed.module.DCSubject;
import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public class SyndFeedPoller {
	
	private Logger Log;
	private MetadataRetrievers metadataRetrievers;
	public SyndFeedPoller() {
		this.Log = Logger.getLogger(SyndFeedPoller.class.getName());
		this.metadataRetrievers = new MetadataRetrievers(DEdConfiguration
				.create());
	}
	/**
	 * Take the SyndEntry "entry", and add or update a corresponding Resource in
	 * our RdfStore.
	 * 
	 * @throws URISyntaxException 
	 */
	protected void addEntry(Feed feed, RdfStore store, SyndEntry entry) {

		// XXX check if the entry exists first...
		String uri = entry.getUri();
		if (uri == null) {
			// Well, that sucks. What kind of lame feed entry has no URI? Regardless, ditch it.
			Log.warn("For some reason, I ran into a feed entry with a null URI. How bizarre. Skipping it.");
			return;
		}
		Resource r = new Resource(URI.create(uri));

		// Back when SyndFeed parsed the feed, it read in from the feed
		// all of the metadata it could find for this URI. Now it has
		// made that metadata available in the object "entry".

		// In fact, this feed is one of the resource's "sources".
		// So let's add this feed to the resource's list of sources.
		r.getSources().add(feed);
		r.setTitle(entry.getTitle());

		// If the resource doesn't have a description, set it to the empty
		// string.
		// FIXME: Write a test checking the right behavior here.
		// (Is that meant to be entry.getDescription()?)
		if (r.getDescription() == null) {
			r.setDescription("");
		} else {
			r.setDescription(entry.getDescription().getValue());
		}

		// FIXME: How is this different from dc:category below?
		// Could we learn anything from the feed about the various
		// "categories" this resource belongs in?
		for (Object category : entry.getCategories()) {
			r.getSubjects().add(((SyndCategory) category).getName());
		}

		// add actual Dublin Core metadata using the DC Module
		DCModule dc_metadata = (DCModule) entry.getModule(DCModule.URI);

		// dc:category
		List<DCSubject> subjects = dc_metadata.getSubjects();
		for (DCSubject s : subjects) {
			r.getSubjects().add(s.getValue());
		}

		// dc:type
		List<String> types = dc_metadata.getTypes();
		r.getTypes().addAll(types);

		// dc:format
		List<String> formats = dc_metadata.getFormats();
		r.getFormats().addAll(formats);

		// dc:contributor
		List<String> contributors = dc_metadata.getContributors();
		r.getContributors().addAll(contributors);

		store.saveDeep(r);

		System.err.println("URI: " + r.getUri().toString());
		// Load additional metadata from external sources
		metadataRetrievers.retrieve(r);
		
		store.saveDeep(r);
	} // addEntry
	

	@SuppressWarnings("unchecked")
	public void poll(Feed feed) {
		RdfStore store = RdfStoreFactory.get().forProvenance(feed.getUri().toString());

		URLConnection feed_connection = null;
		try {
			URL url = new URL(feed.getUri().toString());
			feed_connection = url.openConnection();
		} catch (MalformedURLException e) {
			Log.error("Skipping malformed " + feed.getUri().toString(), e);
			return; // nothing useful can happen
		} catch (IOException e) {
			Log.error("Failing to poll " + feed.getUri().toString(), e);
			return; // nothing useful can happen
		}

		SyndFeedInput input = new SyndFeedInput();
		feed_connection.setConnectTimeout(30000);
		feed_connection.setReadTimeout(60000);
		
		SyndFeed rome_feed;
		try {
			rome_feed = input
				.build(new XmlReader(feed_connection));
		} catch (IllegalArgumentException e) {
			Log.error("Dying bizarrely", e);
			return;
		} catch (FeedException e) {
			Log.error("Dying because the feed seems busted", e);
			return;
		} catch (IOException e) {
			Log.error("Dying because the network went kaboom", e);
			return;
		}

		List<SyndEntry> feed_entries = rome_feed.getEntries();

		for (SyndEntry entry : feed_entries) {
			
			// emit an event with the entry information
			this.addEntry(feed, store, entry);
			
		} // for each entry
	}
}
