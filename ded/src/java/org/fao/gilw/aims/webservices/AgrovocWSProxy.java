package org.fao.gilw.aims.webservices;

public class AgrovocWSProxy implements org.fao.gilw.aims.webservices.AgrovocWS {
  private String _endpoint = null;
  private org.fao.gilw.aims.webservices.AgrovocWS agrovocWS = null;
  
  public AgrovocWSProxy() {
    _initAgrovocWSProxy();
  }
  
  private void _initAgrovocWSProxy() {
    try {
      agrovocWS = (new org.fao.gilw.aims.webservices.AgrovocWSServiceLocator()).getAgrovocWS();
      if (agrovocWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)agrovocWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)agrovocWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (agrovocWS != null)
      ((javax.xml.rpc.Stub)agrovocWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.fao.gilw.aims.webservices.AgrovocWS getAgrovocWS() {
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS;
  }
  
  public java.lang.String[] getDefinitions(int termcode, java.lang.String languagecode) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getDefinitions(termcode, languagecode);
  }
  
  public java.lang.String getAgrovocLanguages() throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getAgrovocLanguages();
  }
  
  public java.lang.String getTermcodeByTerm(java.lang.String searchTerm) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermcodeByTerm(searchTerm);
  }
  
  public java.lang.String getTermcodeByTermXML(java.lang.String searchTerm, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermcodeByTermXML(searchTerm, format);
  }
  
  public java.lang.String getTermByLanguage(int termcode, java.lang.String language) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermByLanguage(termcode, language);
  }
  
  // created by jeetendra.singh@fao.org
  
  public java.lang.String getStatus(int termcode, java.lang.String langcode, java.lang.String userlangcode) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getStatus(termcode, langcode, userlangcode);
  }


 public java.lang.String getRelationByTermcodeXML(int termcode, java.lang.String relation, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getRelationByTermcodeXML(termcode, relation, format);
  }


 public java.lang.String complexSearchByTermXML(java.lang.String searchString, java.lang.String seprator, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.complexSearchByTermXML(searchString, seprator, format);
  }


  
  public java.lang.String getTermByLanguageXML(int termcode, java.lang.String language, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermByLanguageXML(termcode, language, format);
  }
  
  public java.lang.String getTermsListByLanguage(java.lang.String termcodes_list, java.lang.String language) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermsListByLanguage(termcodes_list, language);
  }
  
  public java.lang.String getTermsListByLanguage2(java.lang.String termcodes_list, java.lang.String language, java.lang.String separator) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermsListByLanguage2(termcodes_list, language, separator);
  }
  
  public java.lang.String getTermsListByLanguageXML(java.lang.String termcodes_list, java.lang.String language, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermsListByLanguageXML(termcodes_list, language, format);
  }
  
  public java.lang.String getAllLabelsByTermcode(int termcode) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getAllLabelsByTermcode(termcode);
  }
  
  public java.lang.String getAllLabelsByTermcode2(int termcode, java.lang.String separator) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getAllLabelsByTermcode2(termcode, separator);
  }
  
  public java.lang.String getAllLabelsByTermcodeXML(int termcode, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getAllLabelsByTermcodeXML(termcode, format);
  }
  
  public java.lang.String simpleSearchByMode(java.lang.String searchString, java.lang.String searchmode) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.simpleSearchByMode(searchString, searchmode);
  }
  
  public java.lang.String simpleSearchByMode2(java.lang.String searchString, java.lang.String searchmode, java.lang.String separator) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.simpleSearchByMode2(searchString, searchmode, separator);
  }
  
  //By jeetendra singh
  
  public java.lang.String simpleSearchByModeandLang(java.lang.String searchString, java.lang.String searchmode, java.lang.String separator, java.lang.String language) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.simpleSearchByModeandLang(searchString, searchmode, separator, language);
  }
  
  public java.lang.String simpleSearchByModeXML(java.lang.String searchString, java.lang.String searchmode, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.simpleSearchByModeXML(searchString, searchmode, format);
  }
  
  // By jeetendrasingh@fao.org
  
  public java.lang.String simpleSearchByModeLangXML(java.lang.String searchString, java.lang.String searchmode, java.lang.String format, java.lang.String language) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.simpleSearchByModeLangXML(searchString, searchmode, format, language);
  }
  
  public java.lang.String searchByTerm(java.lang.String searchString) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.searchByTerm(searchString);
  }
  
  public java.lang.String searchByTerm2(java.lang.String searchString, java.lang.String separator) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.searchByTerm2(searchString, separator);
  }
  
  public java.lang.String searchByTermXML(java.lang.String searchString, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.searchByTermXML(searchString, format);
  }
  
  public java.lang.String searchCategoryByMode(java.lang.String searchString, java.lang.String lang, java.lang.String schemeid, java.lang.String searchmode, java.lang.String separator) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.searchCategoryByMode(searchString, lang, schemeid, searchmode, separator);
  }
  
  public java.lang.String searchCategoryByModeXML(java.lang.String searchString, java.lang.String searchmode, java.lang.String schemeid, java.lang.String lang, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.searchCategoryByModeXML(searchString, searchmode, schemeid, lang, format);
  }
  
  public java.lang.String[] getConceptByTerm(java.lang.String searchTerm) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getConceptByTerm(searchTerm);
  }
  
  public java.lang.String[] getConceptInfoByTermcode(java.lang.String termcodestring) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getConceptInfoByTermcode(termcodestring);
  }
  
  public java.lang.String getConceptInfoByTermcodeXML(java.lang.String termcodestring, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getConceptInfoByTermcodeXML(termcodestring, format);
  }
  
  public java.lang.String getDefinitionsXML(int termcode, java.lang.String languagecode, java.lang.String format) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getDefinitionsXML(termcode, languagecode, format);
  }
  
  public java.lang.String getTermExpansion(java.lang.String query, java.lang.String langCode) throws java.rmi.RemoteException{
    if (agrovocWS == null)
      _initAgrovocWSProxy();
    return agrovocWS.getTermExpansion(query, langCode);
  }
  
  
}