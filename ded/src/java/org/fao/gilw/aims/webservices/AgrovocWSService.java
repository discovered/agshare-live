/**
 * AgrovocWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package org.fao.gilw.aims.webservices;

public interface AgrovocWSService extends javax.xml.rpc.Service {
    public java.lang.String getAgrovocWSAddress();

    public org.fao.gilw.aims.webservices.AgrovocWS getAgrovocWS() throws javax.xml.rpc.ServiceException;

    public org.fao.gilw.aims.webservices.AgrovocWS getAgrovocWS(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
