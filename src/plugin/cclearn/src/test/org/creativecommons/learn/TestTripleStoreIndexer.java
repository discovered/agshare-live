package org.creativecommons.learn;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.searcher.HitDetails;
import org.creativecommons.learn.oercloud.Curator;
import org.creativecommons.learn.oercloud.Feed;
import org.creativecommons.learn.oercloud.Resource;
import org.jmock.Expectations;

import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.vocabulary.RDF;

import org.jmock.lib.legacy.ClassImposteriser;

public class TestTripleStoreIndexer extends DiscoverEdTestCase {
	/* ************************** Unit-esque tests ****************/
	
	public static void testGenerateAllPossibleColumnNames() {
		// first, create a Triple (admittedly, in the siteConfigurationStore) that
		// has a predicate that's attached to a Resource.
		Resource r = new Resource(URI.create("http://example.com/#resource"));
		r.setTitle("A title");
		RdfStore store = RdfStoreFactory.get().forDEd();
		store.save(r);
		
		// Now, ask the TripleStoreIndexer what column names it has.		
		TripleStoreIndexer indexer = new TripleStoreIndexer();
		
		Collection<String> got = indexer.getAllPossibleFieldNames();
		assertTrue(got.contains(RdfStoreFactory.get().getOrCreateTablePrefixFromURIAsInteger(RdfStoreFactory.SITE_CONFIG_URI) + "__dct_title"));
	}

    public static void testAddSearchableColumnViaConfigurationFile() {
        final String customLuceneFieldName = "educationlevel";
        final String customPredicateURI = "http://example.com/#educationLevel";
        final String customPredicateValue = "xyz";

        /* first, create a configuration file saying that educationlevel:xyz is
         * supposed to search for Resources with a triple of
         * http://example.com/#educationLevel set to "xyz"
         * ---------------------------------------------- */
    	
        TripleStoreIndexer indexer = new TripleStoreIndexer();
        Configuration customFieldsConfiguration = indexer.getCustomFieldConfiguration();
        customFieldsConfiguration.set(customLuceneFieldName, customPredicateURI);
        RdfStore site_store = RdfStoreFactory.get().forDEd();
         
        /* second, create such a Resource
         * ------------------------------ */
        Curator c = new Curator(URI.create("http://example.com/#i_am_a_curator"));
        Feed f = new Feed(URI.create("http://example.com/#i_am_a_feed"));
        f.setCurator(c);

        site_store.save(c);
        site_store.save(f);

        Resource r = new Resource(URI.create("http://example.com/#i_am_a_resource"));
        // Make the resource's education level = xyz
        Model m = site_store.getModel();
        Property edLevel = m.createProperty(customPredicateURI);
        Literal xyz = m.createLiteral("xyz");
        r.addField(edLevel, xyz);
        r.getSources().add(f);
        RdfStoreFactory.get().forProvenance(f.getUri().toString()).save(r);

        /* third, can the triple store indexer find this custom field and its
         * values?
         * ----------------------- */
        Collection<String> values = indexer.getValuesForCustomLuceneFieldName(
                r.getUri().toString(), customLuceneFieldName);
        System.out.println(values);
        assertTrue(values.contains(customPredicateValue));
        
        /* will it properly declare the custom field name to Lucene? */
        assertTrue(indexer.getAllPossibleFieldNames().contains(customLuceneFieldName));
    }

    /* fourth, verify there is a Lucene column called "educationLevel" in a
     * Lucene/Nutch document corresponding to the Resource we created in
     * step 2 */

    // TODO: in a separate test, run the test above, and add an extra step:
    // do a search for educationLevel:xyz and find it

	/* ************************ Integration-esque tests ****************/
    public void testAllSubjectsInResultHelper() {
        setImposteriser(ClassImposteriser.INSTANCE);
		// Set up: mock out listProvenanceGraphs()
        
        class FakeFactory extends RdfStoreFactory {
        	public Collection<String> getCollectionOfProvenanceGraphsURIs() {
                Collection <String> cookedProvenances = new ArrayList<String>();
                cookedProvenances.add("http://example.com/#feed1");
                cookedProvenances.add("http://example.com/#feed2");
        		return cookedProvenances;
        	}
        	public FakeFactory() {
        		
        	}
        } 
        
        // Let's pretend there is data in the Lucene store, and we passed in
        // enough info so that it could possibly be found
        String[] fields = new String[2];
        fields[0] = IndexFieldName.makeCompleteFieldNameWithProvenance(
				"http://example.com/#feed1", Search.TAGS_PREDICATE_URI);
        fields[1] = IndexFieldName.makeCompleteFieldNameWithProvenance(
				"http://example.com/#feed2", Search.TAGS_PREDICATE_URI);
        String[] values = new String[2];
        values[0] = "value";
        values[1] = "othervalue";
        HashSet<String> expected = new HashSet<String>();
        expected.add(values[0]);
        expected.add(values[1]);

        HitDetails details = new HitDetails(fields, values);
        
        FakeFactory myFakeFactory = new FakeFactory();
        HashSet<String> result = new HashSet<String>(ResultHelper.getValuesFromAllProvenances(
        		myFakeFactory, details, Search.TAGS_PREDICATE_URI));
        assertEquals(expected, result);
    }
    
}
